This IGB App creates an IGB QuickLoad from the custom genome data in IGB.

### Use this App to:

* Save custom genomes as IGB QuickLoads so you can easily return to your data after closing IGB.

### Using QuickLoad Saver

1. Install the App.
2. Select a genome and add any data you would like to include as part of the QuickLoad.
   * Changes to attributes such as track color and load mode will be saved as part of the QuickLoad.
3. Select **File** > **Save Custom Genome to Local QuickLoad Site**.
4. Select a location on your computer to save the QuickLoad folder.

### More info

* IGB QuickLoads can be added to IGB by selecting **File** > **Preferences...** and then selecting the **Data Sources** tab and clicking **Add...**
* Once a QuickLoad has been added to IGB, any genomes or data included as part of the QuickLoad will persist between IGB sessions. This is a great way to include custom genomes or data in IGB and not have to add them every time you start IGB again.
* For more information about IGB QuickLoads please see the IGB User's Guide: https://wiki.bioviz.org/confluence/

### Credits
Noor Zahara developed the Quickload Saver App. Nowlan Freese provided technical, scientific, and design advice. Rachel Weidenhammer conducted testing of the app.